# vim-epub

**Github users : I’m leaving GitHub (because of Microsoft Acquisition).
Read [this](https://github.com/etnadji/301) for the new URL.
This repo will be ERASED soon.**

**Utilisateurs de Github : Je quitte GitHub (suite à son acquisition par Microsoft).
Lisez [ceci](https://github.com/etnadji/301) pour avoir la nouvelle URL.
Ce dépôt sera bientôt SUPPRIMÉ.**

A Vim plugin for EPUB edition.
Released under GNU GPL 3 (check LICENSE file).

With Vim, you [can already edit an epub file](https://github.com/etnadji/vim-epub/wiki/How-Vim-EPUB-open-EPUB-files) and 
save your modifications.

But you can only edit and save the files that already exists : you 
can't add files and do many other things.

**vim-epub** exists to make you free of stupid tasks like extracting 
the EPUB, add the file you want to add, recompress in 2-steps the EPUB, re-open Vim.

## Installation

### Supported operating systems

#### Unices

- BSDs
- Mac OS
- GNU/Linux distributions

#### Windows

To open EPUB files with Vim EPUB on Windows, you have to 
install [GNU unzip](http://gnuwin32.sourceforge.net/packages/zip.htm) in the executable path.

### Vim

You need a copy of Vim compiled with python support.

### Needed programs

You need the `unzip` programm in order to make zip.vim (built-in) work.
It is frequently installed by default on newbies distributions like Ubuntu, Linux Mint, etc, and it may be installed already because of other softwares dependencies.

In Debian-like GNU/Linux distributions:

`sudo apt-get install unzip`

You also needs `pandoc` program.

Again:

`sudo apt-get install pandoc`

### Plugin managers

#### [vim-plug](https://github.com/junegunn/vim-plug)
  - Add `Plug 'https://github.com/etnadji/vim-epub'` to .vimrc
  - Run `:PlugInstall`

#### [Vundle](https://github.com/gmarik/vundle)
  - Add `Bundle 'etnadji/vim-epub'` or `Bundle 'https://github.com/etnadji/vim-epub'` to .vimrc
  - Run `:BundleInstall`

#### [Pathogen](https://github.com/tpope/vim-pathogen)
  - `git clone https://github.com/etnadji/vim-epub ~/.vim/bundle/vim-epub`

#### [NeoBundle](https://github.com/Shougo/neobundle.vim)
  - Add `NeoBundle 'https://github.com/etnadji/vim-epub'` to .vimrc
  - Run `:NeoBundleInstall`

## Notable features

- Adding medias (XHTML, CSS, fonts, images…)
- Add a _Table of contents_ page.
- Provide a menu to easily link XHTML and CSS contents.
- Merge, rename files embedded in the EPUB
- Check the differences between the current EPUB and another.

### And more:

The full list of features / commands is available on the [wiki](https://github.com/etnadji/vim-epub/wiki/Features) or doc files ([en](https://github.com/etnadji/vim-epub/blob/master/doc/vim-epub.txt) /[fr](https://github.com/etnadji/vim-epub/blob/master/doc/vim-epub-french.frx)):
`:help vim-epub.txt` or `:help vim-epub-french.frx`

## Contribute!

See TODO.md.
